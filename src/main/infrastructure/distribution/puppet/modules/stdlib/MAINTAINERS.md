## Maintenance

Maintainers:
  - Puppet Forge Modules Team `forge-modules |at| distribution.puppet |dot| com`

Tickets: https://tickets.distribution.puppet.com/browse/MODULES. Make sure to set component to `stdlib`.
