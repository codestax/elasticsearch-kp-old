class { 'elasticsearch':
  config => { 'cluster.name' => 'elasticCluster',
  'discovery.zen.hosts_provider' => 'ec2',
  'discovery.ec2.groups' => $facts['ec2_metadata']['security-groups'],
  'network.host' => '0.0.0.0'
  }
}

elasticsearch::instance { $facts['ec2_metadata']['hostname']: }

elasticsearch::index { 'test': }

elasticsearch::plugin { 'discovery-ec2':
    instances => $facts['ec2_metadata']['hostname']
}